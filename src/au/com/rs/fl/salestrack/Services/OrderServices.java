/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack.Services;

import java.util.ArrayList;
import java.util.List;

import au.com.rs.fl.salestrack.models.ClientVisit;
import au.com.rs.fl.salestrack.models.Customer;
import au.com.rs.fl.salestrack.models.Order;
import au.com.rs.fl.salestrack.models.OrderView;
import au.com.rs.fl.salestrack.models.Product;
import au.com.rs.fl.salestrack.repositories.IClientVisitRepository;
import au.com.rs.fl.salestrack.repositories.ICustomerRepository;
import au.com.rs.fl.salestrack.repositories.IOrderRepository;
import au.com.rs.fl.salestrack.repositories.IProductRepository;

public class OrderServices {

	private IOrderRepository orderRep;
	private IClientVisitRepository clientVisitRep;
	private IProductRepository productRep;
	private ICustomerRepository customerRep;

	public OrderServices(IOrderRepository orderRepository,
			IClientVisitRepository clientVisitRep,
			IProductRepository productRep, ICustomerRepository customerRep) {
		this.orderRep = orderRepository;
		this.clientVisitRep = clientVisitRep;
		this.productRep = productRep;
		this.customerRep = customerRep;
	}

	public List<OrderView> get(long dateId) {
		List<OrderView> orderViews = new ArrayList<OrderView>();
		List<Order> orders = orderRep.get(dateId);
		for (Order o : orders) {
			orderViews.add(convert(o));
		}
		return orderViews;
	}

	public long Add(OrderView order) {
		long id = orderRep.Add(convert(order));
		return id;
	}

	private Order convert(OrderView orderView) {
		Order order = new Order();
		order.setClientVisitId(orderView.getId());
		order.setComment(orderView.getComment());
		order.setCustomerId(orderView.getCustomer().getId());
		order.setProductId(orderView.getProduct().getId());
		return order;
	}

	private OrderView convert(Order order) {
		OrderView orderView = new OrderView();
		long visitId = order.getClientVisitId();
		long productId = order.getProductId();
		long customerId = order.getCustomerId();
		String comment = order.getComment();
		ClientVisit clientVisit = clientVisitRep.get(visitId);
		Product product = productRep.get(productId);
		Customer customer = customerRep.get(customerId);

		orderView.setId(order.getId());
		orderView.setClientVisit(clientVisit);
		orderView.setProduct(product);
		orderView.setCustomer(customer);
		orderView.setComment(comment);
		return orderView;
	}
}
