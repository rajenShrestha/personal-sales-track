/**
 * Copyright 2015 Rajen Shrestha
 * All right are reserved. Reproduction or transmission in whole or in 
 * part, in any form or by any means, electronic, mechanical or otherwise
 * is published without the prior written consent of the copyright owner.
 */
package au.com.rs.fl.salestrack;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import au.com.rs.fl.salestrack.Services.OrderServices;
import au.com.rs.fl.salestrack.models.ClientVisit;
import au.com.rs.fl.salestrack.repositories.IClientVisitRepository;
import au.com.rs.fl.salestrack.repositories.ICustomerRepository;
import au.com.rs.fl.salestrack.repositories.IOrderRepository;
import au.com.rs.fl.salestrack.repositories.IProductRepository;
import au.com.rs.fl.salestrack.repositories.test.TestClientVisitRepository;
import au.com.rs.fl.salestrack.repositories.test.TestCustomerRepository;
import au.com.rs.fl.salestrack.repositories.test.TestOrderRepository;
import au.com.rs.fl.salestrack.repositories.test.TestProductRepository;

public abstract class AbstractSaleDetailFragment extends Fragment {

	protected ClientVisit clientVisit;
	protected OrderServices orderServices;
	
	private IOrderRepository orderRep = new TestOrderRepository();
	private IClientVisitRepository clientVisitRep = new TestClientVisitRepository();
	private IProductRepository productRep = new TestProductRepository();
	private ICustomerRepository customerRep = new TestCustomerRepository();

	public AbstractSaleDetailFragment() {
		orderServices = new OrderServices(orderRep, clientVisitRep, productRep,
				customerRep);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle b = this.getActivity().getIntent()
				.getBundleExtra(ClientVisit.BUNDLE_ID);
		if (b != null) {
			clientVisit = ClientVisit.getSales(b);
		}
	}
}
