/**
 * Copyright 2015 Rajen Shrestha
 * All right are reserved. Reproduction or transmission in whole or in 
 * part, in any form or by any means, electronic, mechanical or otherwise
 * is published without the prior written consent of the copyright owner.
 */
package au.com.rs.fl.salestrack;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import au.com.rs.fl.salestrack.models.OrderView;
import au.com.rs.fl.salestrack.models.Product;

/**
 * fragment contains customer name, product list
 * 
 * @author rshrestha
 *
 */
public class SaleDetailFragment extends AbstractSaleDetailFragment {

	public SaleDetailFragment() {
		// Required empty public constructor
		super();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_sale_detail,
				container, false);

		TextView customerNameText = (TextView) rootView
				.findViewById(R.id.editTextCustomerName);

		List<Product> products = new ArrayList<Product>();
		ArrayAdapter<Product> adapter = new ArrayAdapter<Product>(
				getActivity(), 0, products);
		ListView productListView = (ListView) rootView
				.findViewById(R.id.listViewProduct);
		
		EditText comment = (EditText)rootView.findViewById(R.id.editTextComment);

		if (clientVisit != null) {
			List<OrderView> oViewList = orderServices.get(clientVisit.getId());
			if (oViewList.size() > 1) {
				// may be different view
				// show different view if there are more than one customer
				
				//TODO:consister only one customer in the return
			} else {
				OrderView oView = oViewList.get(0);
				// loss the OOD principle "Only trust to close friend"
				customerNameText.setText(oView.getCustomer().getName());
				products.add(oView.getProduct());
				productListView.setAdapter(adapter);
				comment.setText(oView.getComment());

			}

		}

		return rootView;
	}
}
