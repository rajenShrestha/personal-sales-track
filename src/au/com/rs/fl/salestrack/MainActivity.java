/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import au.com.rs.fl.salestrack.SalesListFragment.OnMainListFragmentInteractionListener;
import au.com.rs.fl.salestrack.models.ClientVisit;

public class MainActivity extends ActionBarActivity implements
		OnMainListFragmentInteractionListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Fragment is hold by this Activity Implementation of
	 * OnMainListFragmentInteractionListener
	 */
	@Override
	public void selectedSale(ClientVisit sale) {
		Intent orderDetail = new Intent(this.getApplicationContext(),
				SaleDetailActivity.class);
		orderDetail.putExtra(ClientVisit.BUNDLE_ID, sale.getBundle());
		startActivityForResult(orderDetail, 1001);

	}

}
