/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class SaleDetailFutureFragment extends AbstractSaleDetailFragment {

	public SaleDetailFutureFragment() {
		// Required empty public constructor
		super();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_sale_detail_future,
				container, false);
		if (clientVisit != null) {
			Toast toast = Toast.makeText(getActivity(), clientVisit.getDate()
					+ " " + clientVisit.getId(), Toast.LENGTH_SHORT);
			toast.show();
		}

		return rootView;
	}

}
