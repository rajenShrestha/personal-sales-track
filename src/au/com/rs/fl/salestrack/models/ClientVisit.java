/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack.models;

import android.os.Bundle;

/**
 * this model represents a visit or a sale
 * 
 * @author rshrestha
 *
 */
public class ClientVisit {
	public static final String BUNDLE_ID = "ClientVisit_Bundle";

	private long id;
	private String date;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Bundle getBundle() {
		Bundle b = new Bundle();
		b.putLong("id", id);
		b.putString("date", date);

		return b;
	}

	public static ClientVisit getSales(Bundle b) {
		ClientVisit cv = new ClientVisit();
		cv.setId(b.getLong("id"));
		cv.setDate(b.getString("date"));
		return cv;
	}

}
