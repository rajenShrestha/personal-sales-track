/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack.models;

public class Category {
	private long id;
	private String name;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Id: " + id + " name: " + name;
	}
}
