/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack.models;

public class OrderView {
	private long id;
	private ClientVisit clientVisit;
	private Product product;
	private Customer customer;
	private String comment;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public ClientVisit getClientVisit() {
		return clientVisit;
	}
	public void setClientVisit(ClientVisit clientVisit) {
		this.clientVisit = clientVisit;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}


