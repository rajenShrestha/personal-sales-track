/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import au.com.rs.fl.salestrack.R;
import au.com.rs.fl.salestrack.models.ClientVisit;

public class SalesDateAdapter extends ArrayAdapter<ClientVisit> {

	private Context context;
	private List<ClientVisit> salesDates;

	public SalesDateAdapter(Context context, int resource,
			List<ClientVisit> objects) {
		super(context, resource, objects);
		this.salesDates = objects;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ClientVisit cv = salesDates.get(position);
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		View rootView = inflater.inflate(R.layout.sales_list_item, null);
		TextView text = (TextView) rootView.findViewById(R.id.saleDate);
		text.setText(cv.getDate());
		TextView idText = (TextView) rootView.findViewById(R.id.orderId);
		idText.setText(Long.toString(cv.getId()));
		return rootView;
	}
}
