/**
 * Copyright 2015 Rajen Shrestha
 * All right are reserved. Reproduction or transmission in whole or in 
 * part, in any form or by any means, electronic, mechanical or otherwise
 * is published without the prior written consent of the copyright owner.
 */
package au.com.rs.fl.salestrack.repositories.test;

import java.util.ArrayList;
import java.util.List;

import au.com.rs.fl.salestrack.models.Product;
import au.com.rs.fl.salestrack.repositories.IProductRepository;

public class TestProductRepository implements IProductRepository {
	List<Product> products = new ArrayList<Product>();

	public TestProductRepository() {
		createProduct();
	}
	@Override
	public List<Product> getAll() {		
		return products;
	}

	@Override
	public long add(Product product) {
		int id = products.size() + 1;
		product.setId(id);
		products.add(product);
		return id;
	}

	@Override
	public Product get(long id) {
		Product p = new Product();
		for (Product product : products) {
			if(product.getId() == id){
				p = product;
			}
		}
		return p;
	}

	private void createProduct() {
		int id = 1;
		
		for (int index = 0; index < 101; index++) {
			Product p = new Product();
			p.setId(id);
			p.setCategoryId(id);
			p.setMadeCountry("Australia");
			p.setName(id + " " + "product");
			p.setPrice(1.0);
			id++;
			products.add(p);
		}
	}
}
