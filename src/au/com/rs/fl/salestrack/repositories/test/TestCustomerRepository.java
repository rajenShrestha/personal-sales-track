/**
 * Copyright 2015 Rajen Shrestha
 * All right are reserved. Reproduction or transmission in whole or in 
 * part, in any form or by any means, electronic, mechanical or otherwise
 * is published without the prior written consent of the copyright owner.
 */
package au.com.rs.fl.salestrack.repositories.test;

import java.util.ArrayList;
import java.util.List;

import au.com.rs.fl.salestrack.models.Customer;
import au.com.rs.fl.salestrack.repositories.ICustomerRepository;

public class TestCustomerRepository implements ICustomerRepository {

	List<Customer> customers = new ArrayList<Customer>();

	public TestCustomerRepository() {
		createCustomer();
	}

	@Override
	public List<Customer> getAll() {
		return customers;
	}

	@Override
	public long add(Customer customer) {
		long id = customers.size() + 1;
		customer.setId(id);
		customers.add(customer);
		return id;
	}

	@Override
	public Customer get(long id) {
		Customer c = new Customer();
		for (Customer customer : customers) {
			if (customer.getId() == id) {
				c = customer;
			}
		}
		return c;
	}

	@Override
	public Customer get(String email) {
		Customer c = new Customer();
		for (Customer customer : customers) {
			if (customer.getEmailAddress() == email) {
				c = customer;
			}
		}
		return c;
	}

	private void createCustomer() {
		long id = 1;
		for (int index = 0; index < 15; index++) {
			Customer c = new Customer();
			c.setId(id);
			c.setEmailAddress(id + "@gmail.com");
			c.setMailAddress(id + " MailingAddress, Sydney 2000");
			c.setPhoneNumber(99999999 + id);
			id++;
			customers.add(c);
		}
	}
}
