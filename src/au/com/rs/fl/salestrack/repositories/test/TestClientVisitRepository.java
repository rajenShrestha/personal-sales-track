/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack.repositories.test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.util.Log;
import au.com.rs.fl.salestrack.models.ClientVisit;
import au.com.rs.fl.salestrack.repositories.IClientVisitRepository;

public class TestClientVisitRepository implements IClientVisitRepository {

	private List<ClientVisit> visits = new ArrayList<ClientVisit>();

	public TestClientVisitRepository() {
		CreateListForTest();
	}

	@Override
	public List<ClientVisit> getAll() {
		return visits;
	}

	@Override
	public List<ClientVisit> getTopHundred() {
		return visits;
	}

	@Override
	public long add(ClientVisit clientVisit) {
		visits.add(clientVisit);
		return 1;
	}

	@Override
	public ClientVisit get(long id) {
		ClientVisit cv = new ClientVisit();
		for (ClientVisit clientVisit : visits) {
			if (id == clientVisit.getId()) {
				cv = clientVisit;
			}
		}
		return cv;
	}

	private void CreateListForTest() {
		// TODO: Just for Test:
		long id = 0;
		String date = "25/01/2014";
		ClientVisit cv = new ClientVisit();
		for (int index = 0; index < 50; index++) {
			try {
				cv = createClientVisit(id, date);
				date = cv.getDate();
				id = cv.getId();
			} catch (ParseException e) {
				Log.e("Sales Frag", e.getMessage());
			}
			add(cv);
		}
	}

	private ClientVisit createClientVisit(long id, String date)
			throws ParseException {
		ClientVisit cv = new ClientVisit();
		cv.setId(++id);

		DateFormat df = SimpleDateFormat
				.getDateInstance(SimpleDateFormat.SHORT);
		Calendar c = Calendar.getInstance();
		c.setTime(df.parse(date));
		c.add(Calendar.DATE, 1);
		Date resultdate = new Date(c.getTimeInMillis());
		String dateInString = df.format(resultdate);
		cv.setDate(dateInString);

		return cv;
	}

}
