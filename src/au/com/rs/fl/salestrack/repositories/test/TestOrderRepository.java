/**
 * Copyright 2015 Rajen Shrestha
 * All right are reserved. Reproduction or transmission in whole or in 
 * part, in any form or by any means, electronic, mechanical or otherwise
 * is published without the prior written consent of the copyright owner.
 */
package au.com.rs.fl.salestrack.repositories.test;

import java.util.ArrayList;
import java.util.List;

import au.com.rs.fl.salestrack.models.Order;
import au.com.rs.fl.salestrack.repositories.IOrderRepository;

public class TestOrderRepository implements IOrderRepository {

	private List<Order> orders = new ArrayList<Order>();
	
	public TestOrderRepository() {
		createOrder();
	}
	@Override
	public List<Order> get(long dateId) {
		List<Order> o = new ArrayList<Order>();
		for (Order order : orders) {
			if(order.getClientVisitId() == dateId){
				o.add(order);
			}
		}
		return o;
	}

	@Override
	public long Add(Order order) {
		orders.add(order);
		return 0;
	}

	private List<Order> createOrder() {
		
		long id = 0;
		CharSequence message = "";

		for (int index = 0; index < 10; index++) {
			id++;
			message = "This is messsage number " + id;
			orders.add(createOrder(id, message));
		}
		return orders;
	}

	private Order createOrder(long id, CharSequence message) {
		Order o = new Order();
		o.setId(id);
		o.setClientVisitId(id);
		o.setComment(message.toString());
		o.setCustomerId(id);
		o.setProductId(id);
		return o;
	}

}
