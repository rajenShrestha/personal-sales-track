/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack.repositories.test;

import java.util.ArrayList;
import java.util.List;

import au.com.rs.fl.salestrack.models.Category;
import au.com.rs.fl.salestrack.repositories.ICategoryRepository;

public class TestCategoryRepository implements ICategoryRepository {

	List<Category> categories = new ArrayList<Category>();
	
	@Override
	public List<Category> getAll() {
		return categories;
	}

	@Override
	public long add(Category category) {
		int id = categories.size() + 1;
		return id;
	}

	@Override
	public Category get(long id) {
		Category c = new Category();
		for (Category category : categories) {
			if(category.getId() == id){
				c = category;
			}
		}
		return c;
	}

	@Override
	public boolean isExist(String name) {
		
		return false;
	}

	private void createCategoryList(){
		long id = 1;
		for(int index =0; index<101;index++){
			Category c = new Category();
			c.setId(id);
			c.setName(id + " category");
			id++;
		}
	}
}
