/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack.repositories;

import java.util.List;

import au.com.rs.fl.salestrack.models.ClientVisit;

public interface IClientVisitRepository {
	public List<ClientVisit> getAll();
	public List<ClientVisit> getTopHundred();
	public ClientVisit get(long id);
	public long add(ClientVisit clientVisit);
}
