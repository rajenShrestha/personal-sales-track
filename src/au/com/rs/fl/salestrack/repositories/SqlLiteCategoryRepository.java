/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack.repositories;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import au.com.rs.fl.salestrack.models.Category;

public class SqlLiteCategoryRepository extends SalesTrackDb implements
		ICategoryRepository {

	public static String TABLE_NAME = "Category";

	public SqlLiteCategoryRepository(Context context) {
		super(context);
	}

	@Override
	public List<Category> getAll() {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db
				.query(TABLE_NAME, null, null, null, null, null, null);
		List<Category> categories = new ArrayList<Category>();

		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			Category category = createCategory(cursor);
			categories.add(category);
			cursor.moveToNext();
		}
		cursor.close();
		return categories;
	}

	@Override
	public long add(Category category) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("name", category.getName());
		return db.insert(TABLE_NAME, null, values);
	}

	@Override
	public Category get(long id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_NAME, null, "id=" + id, null, null,
				null, null);

		Category category = new Category();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			category = createCategory(cursor);
			cursor.moveToNext();
		}
		cursor.close();
		return category;
	}

	// should i make name as primary key because it must be one
	public boolean isExist(String name) {
		boolean exist = false;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(TABLE_NAME, null, "name=" + name, null, null,
				null, null);
		exist = cursor.getCount() >= 1;

		return exist;
	}

	public static void createTable(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_NAME + " "
				+ "(id integer primary key, name text)");
	}

	public static void dropTable(SQLiteDatabase db) {
		db.execSQL("drop table if exists " + TABLE_NAME);
	}

	// iff new coloumn, then edit this
	private Category createCategory(Cursor cursor) {
		Category category = new Category();
		Long id = cursor.getLong(0);
		String name = cursor.getString(1);
		category.setId(id);
		category.setName(name);
		return category;
	}

}
