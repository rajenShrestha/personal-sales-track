/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack.repositories;

import java.util.List;

import au.com.rs.fl.salestrack.models.Product;

public interface IProductRepository {
	public List<Product> getAll();

	public long add(Product category);

	public Product get(long id);
}
