/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack.repositories;

import java.util.List;

import au.com.rs.fl.salestrack.models.Category;

/**
 * 
 * @author rshrestha
 *
 */
public interface ICategoryRepository {
	public List<Category> getAll();
	
	/**
	 * return id of added category
	 * @param category
	 * @return
	 */
	public long add(Category category);
	public Category get(long id);
	
	public boolean isExist(String name);
}
