/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack.repositories;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * responsible for creating database responsible for checking database
 * responsible for checking table in database
 * 
 * @author rshrestha
 *
 */
public abstract class SalesTrackDb extends SQLiteOpenHelper {
	
	private static final String DATABASE_NAME = "au.com.rs.fl.SalesTrack.db";

	public SalesTrackDb(Context context) {
		super(context, DATABASE_NAME, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {		
		SqlLiteCategoryRepository.createTable(db);
		SqlLiteProductRepository.createTable(db);
		SqlLiteCustomerRepository.createTable(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// need to delete all the tables
		SqlLiteCategoryRepository.dropTable(db);
		SqlLiteProductRepository.dropTable(db);	
		SqlLiteCustomerRepository.dropTable(db);
		
		onCreate(db);		
	}

}
