/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack.repositories;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import au.com.rs.fl.salestrack.models.Customer;

public class SqlLiteCustomerRepository extends SalesTrackDb implements
		ICustomerRepository {

	public static String TABLE_NAME = "Customer";

	private static String COLUMN_NAME = "name";
	private static String COLUMN_PHONE = "phone";
	private static String COLUMN_EMAIL = "email";
	private static String COLUMN_ADDRESS = "address";

	public SqlLiteCustomerRepository(Context context) {
		super(context);
	}

	@Override
	public List<Customer> getAll() {
		List<Customer> customers = new ArrayList<Customer>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = null;
		try {
			cursor = db.query(TABLE_NAME, null, null, null, null, null, null,
					null);
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				Customer customer = createCustomer(cursor);
				customers.add(customer);
				cursor.moveToNext();
			}
		} finally {
			db.close();
			if (cursor != null) {
				cursor.close();
			}
		}
		return customers;
	}

	@Override
	public long add(Customer customer) {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = null;
		try {
			ContentValues values = new ContentValues();
			values.put(COLUMN_NAME, customer.getName());
			values.put(COLUMN_PHONE, customer.getPhoneNumber());
			values.put(COLUMN_EMAIL, customer.getEmailAddress());
			values.put(COLUMN_ADDRESS, customer.getMailAddress());

			db.insert(TABLE_NAME, null, values);
		} finally {
			db.close();
			if (cursor != null) {
				cursor.close();
			}
		}
		return 0;
	}

	@Override
	public Customer get(long id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_NAME, null, "id=" + id, null, null,
				null, null);

		Customer customer = new Customer();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			customer = createCustomer(cursor);
			cursor.moveToNext();
		}
		cursor.close();
		return customer;
	}

	@Override
	public Customer get(String email) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_NAME, null, COLUMN_EMAIL + "=" + email,
				null, null, null, null);

		Customer customer = new Customer();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			customer = createCustomer(cursor);
			cursor.moveToNext();
		}
		cursor.close();
		return customer;
	}

	protected static void createTable(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_NAME + " " + "("
				+ "id integer primary key, " + COLUMN_NAME + " text, "
				+ COLUMN_PHONE + " text ," + COLUMN_EMAIL + " text, "
				+ COLUMN_ADDRESS + " text)");
	}

	protected static void dropTable(SQLiteDatabase db) {
		db.execSQL("drop table if exist " + TABLE_NAME);
	}

	private Customer createCustomer(Cursor cursor) {
		Customer customer = new Customer();
		int indexId = cursor.getColumnIndex("id");
		int indexName = cursor.getColumnIndex(COLUMN_NAME);
		int indexPhone = cursor.getColumnIndex(COLUMN_PHONE);
		int indexEmail = cursor.getColumnIndex(COLUMN_EMAIL);
		int indexAddress = cursor.getColumnIndex(COLUMN_ADDRESS);

		long id = cursor.getLong(indexId);
		String name = cursor.getString(indexName);
		long phone = cursor.getLong(indexPhone);
		String email = cursor.getString(indexEmail);
		String address = cursor.getString(indexAddress);

		customer.setId(id);
		customer.setName(name);
		customer.setPhoneNumber(phone);
		customer.setEmailAddress(email);
		customer.setMailAddress(address);
		return customer;
	}

}
