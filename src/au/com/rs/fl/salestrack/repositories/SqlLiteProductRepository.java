/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack.repositories;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import au.com.rs.fl.salestrack.models.Product;

public class SqlLiteProductRepository extends SalesTrackDb implements
		IProductRepository {

	public static String TABLE_NAME = "Product";

	private static String COLUMN_CATEGORY_ID = "categoryId";
	private static String COLUMN_NAME = "name";
	private static String COLUMN_MADE = "made";
	private static String COLUMN_PRICE = "price";

	public SqlLiteProductRepository(Context context) {
		super(context);
	}

	@Override
	public List<Product> getAll() {
		List<Product> products = new ArrayList<Product>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor rows = null;
		try {
			rows = db.query(TABLE_NAME, null, null, null, null, null, null);
			rows.moveToFirst();
			while (!rows.isAfterLast()) {
				Product product = createProduct(rows);
				products.add(product);
				rows.moveToNext();
			}

		} finally {
			db.close();
			if (rows != null) {
				rows.close();
			}
		}

		return products;
	}

	@Override
	public long add(Product category) {
		SQLiteDatabase db = this.getReadableDatabase();
		long id = 0;
		try {
			ContentValues values = new ContentValues();
			values.put(COLUMN_CATEGORY_ID, category.getCategoryId());
			values.put(COLUMN_NAME, category.getName());
			values.put(COLUMN_MADE, category.getMadeCountry());
			values.put(COLUMN_PRICE, category.getPrice());

			id = db.insert(TABLE_NAME, null, values);
		} finally {
			db.close();
		}
		return id;
	}

	@Override
	public Product get(long id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_NAME, null, "id="+id, null, null, null, null);
		
		Product product = new Product();
		cursor.moveToFirst();
		while(!cursor.isAfterLast()){
			product = createProduct(cursor);
			cursor.moveToNext();
		}
		cursor.close();
		return product;
	}

	public static void createTable(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_NAME + " "
				+ "(id integer primary key, " + COLUMN_CATEGORY_ID
				+ " integer, " + COLUMN_NAME + " text, " + COLUMN_MADE
				+ " text, " + COLUMN_PRICE + " numeric, " + "foreign key ("
				+ COLUMN_CATEGORY_ID + ") references Category (id))");
	}

	public static void dropTable(SQLiteDatabase db) {
		db.execSQL("drop table if exists " + TABLE_NAME);
	}

	private Product createProduct(Cursor cursor) {
		Product product = new Product();

		int indexId = cursor.getColumnIndex("id");
		int indexCategoryId = cursor.getColumnIndex(COLUMN_CATEGORY_ID);
		int indexName = cursor.getColumnIndex(COLUMN_NAME);
		int indexMade = cursor.getColumnIndex(COLUMN_MADE);
		int indexPrice = cursor.getColumnIndex(COLUMN_PRICE);

		long id = cursor.getLong(indexId);
		long categoryId = cursor.getLong(indexCategoryId);
		String name = cursor.getString(indexName);
		String made = cursor.getString(indexMade);
		double price = cursor.getDouble(indexPrice);

		product.setId(id);
		product.setCategoryId(categoryId);
		product.setName(name);
		product.setMadeCountry(made);
		product.setPrice(price);

		return product;
	}

}
