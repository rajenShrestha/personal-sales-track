/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack.repositories;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import au.com.rs.fl.salestrack.models.ClientVisit;

public class SqlLiteClientVisitRepository extends SalesTrackDb implements
		IClientVisitRepository {

	private static String TAG = "Personal Sales Track: Client Visit";

	public static final String TABLE_NAME = "ClientVisit";
	private static final String COLUMN_DATE = "date";

	public SqlLiteClientVisitRepository(Context context) {
		super(context);
	}

	@Override
	public List<ClientVisit> getAll() {
		List<ClientVisit> clientVisits = new ArrayList<ClientVisit>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor rows = null;
		try {
			rows = db.query(TABLE_NAME, null, null, null, null, null,
					COLUMN_DATE + " desc");
			rows.moveToFirst();
			while (!rows.isAfterLast()) {
				ClientVisit clientVisit = createClientVisit(rows);
				clientVisits.add(clientVisit);
				rows.moveToNext();
			}
		} finally {
			db.close();
			if (rows != null) {
				rows.close();
			}
		}
		return clientVisits;
	}

	@Override
	public List<ClientVisit> getTopHundred() {
		List<ClientVisit> clientVisits = new ArrayList<ClientVisit>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor rows = null;
		try {
			rows = db.rawQuery("select * from " + TABLE_NAME + " order by "
					+ COLUMN_DATE + " desc limit 100", null);
			rows.moveToFirst();
			while (!rows.isAfterLast()) {
				ClientVisit clientVisit = createClientVisit(rows);
				clientVisits.add(clientVisit);
				rows.moveToNext();
			}

		} finally {
			db.close();
			if (rows != null) {
				rows.close();
			}
		}
		return clientVisits;
	}

	@Override
	public ClientVisit get(long id) {
		ClientVisit visit = new ClientVisit();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = null;

		try {
			cursor = db.query(TABLE_NAME, null, "id=" + id, null, null, null,
					null);
			cursor.moveToFirst();
			visit = createClientVisit(cursor);
		} finally {
			db.close();
			if (cursor != null) {
				cursor.close();
			}
		}
		return visit;
	}

	@Override
	public long add(ClientVisit clientVisit) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(COLUMN_DATE, clientVisit.getDate());
		return db.insert(TABLE_NAME, null, values);
	}

	public static void createTable(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_NAME + " "
				+ "(id integer primary key, date text)");
	}

	public static void dropTable(SQLiteDatabase db) {
		db.execSQL("drop table if exists " + TABLE_NAME);
	}

	// iff new coloumn, then edit this
	private ClientVisit createClientVisit(Cursor cursor) {
		ClientVisit clientVisit = new ClientVisit();
		int indexId = cursor.getColumnIndex("id");
		int indexDate = cursor.getColumnIndex(COLUMN_DATE);

		long id = cursor.getLong(indexId);
		String dateString = cursor.getString(indexDate);
		clientVisit.setDate(dateString);
		return clientVisit;
	}
}
