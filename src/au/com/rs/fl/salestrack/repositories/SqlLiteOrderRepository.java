/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack.repositories;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import au.com.rs.fl.salestrack.models.Order;

public class SqlLiteOrderRepository extends SalesTrackDb implements
		IOrderRepository {

	public static final String TABLE_NAME = "Order";

	private static final String COLUMN_VISIT_ID = "clientVisitId";
	private static final String COLUMN_PRODUCT_ID = "productId";
	private static final String COLUMN_CUSTOMER_ID = "customerId";
	private static final String COLUMN_COMMENT = "comment";

	public SqlLiteOrderRepository(Context context) {
		super(context);
	}

	@Override
	/**
	 * get order by date;
	 */
	public List<Order> get(long visitId) {
		List<Order> orders = new ArrayList<Order>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = null;
		try {
			cursor = db.query(TABLE_NAME, null,
					COLUMN_VISIT_ID + "=" + visitId, null, null, null, null);

			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				Order order = createOrder(cursor);
				orders.add(order);
				cursor.moveToNext();
			}
		} finally {
			db.close();
			if (cursor != null) {
				cursor.close();
			}
		}

		return orders;
	}

	@Override
	public long Add(Order order) {
		long id = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			ContentValues values = new ContentValues();
			values.put(COLUMN_VISIT_ID, order.getClientVisitId());
			values.put(COLUMN_PRODUCT_ID, order.getProductId());
			values.put(COLUMN_CUSTOMER_ID, order.getCustomerId());
			values.put(COLUMN_COMMENT, order.getComment());
			db.insert(TABLE_NAME, null, values);
		}
		finally{
			db.close();
		}
		return id;
	}

	public static void createTable(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_NAME + " "
				+ "(id integer primary key, " + COLUMN_COMMENT + " text, "
				+ "foreign key (" + COLUMN_VISIT_ID + ") references "
				+ SqlLiteClientVisitRepository.TABLE_NAME + " (id), "
				+ "foreign key (" + COLUMN_PRODUCT_ID + ") references "
				+ SqlLiteProductRepository.TABLE_NAME + " (id), "
				+ "foreign key (" + COLUMN_CUSTOMER_ID + ") references "
				+ SqlLiteCustomerRepository.TABLE_NAME + " (id) )");
	}

	public static void dropTable(SQLiteDatabase db) {
		db.execSQL("drop table if exists " + TABLE_NAME);
	}

	private Order createOrder(Cursor cursor) {
		Order order = new Order();

		int indexId = cursor.getColumnIndex("id");
		int indexVisitId = cursor.getColumnIndex(COLUMN_VISIT_ID);
		int indexProductId = cursor.getColumnIndex(COLUMN_PRODUCT_ID);
		int indexCustomerId = cursor.getColumnIndex(COLUMN_CUSTOMER_ID);
		int indexComment = cursor.getColumnIndex(COLUMN_COMMENT);

		long id = cursor.getLong(indexId);
		long visitId = cursor.getLong(indexVisitId);
		long productId = cursor.getLong(indexProductId);
		long customerId = cursor.getLong(indexCustomerId);
		String comment = cursor.getString(indexComment);

		order.setId(id);
		order.setClientVisitId(visitId);
		order.setProductId(productId);
		order.setCustomerId(customerId);
		order.setComment(comment);

		return order;
	}

}
