/**
* Copyright 2015 Rajen Shrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
*/
package au.com.rs.fl.salestrack;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import au.com.rs.fl.salestrack.adapter.SalesDateAdapter;
import au.com.rs.fl.salestrack.models.ClientVisit;
import au.com.rs.fl.salestrack.repositories.IClientVisitRepository;
import au.com.rs.fl.salestrack.repositories.test.TestClientVisitRepository;

/**
 * 
 * @author rshrestha
 *
 */
public class SalesListFragment extends Fragment {

	private OnMainListFragmentInteractionListener mainActivity;
	// TODO: Just for Test: change the repository
	private IClientVisitRepository cvRepository = new TestClientVisitRepository();

	public SalesListFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_sales_list,
				container, false);

		List<ClientVisit> cvList = cvRepository.getAll();
		SalesDateAdapter adapter = new SalesDateAdapter(getActivity(),
				R.layout.sales_list_item, cvList);

		ListView salesListView = (ListView) rootView
				.findViewById(R.id.listSales);
		salesListView.setAdapter(adapter);
		salesListView.setOnItemClickListener(new SaleItemClick());

		return rootView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mainActivity = (OnMainListFragmentInteractionListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mainActivity = null;
	}

	private class SaleItemClick implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {

			TextView orderId = (TextView) view.findViewById(R.id.orderId);
			TextView date = (TextView) view.findViewById(R.id.saleDate);
			ClientVisit cv = new ClientVisit();
			cv.setId(Long.parseLong((String) orderId.getText()));
			cv.setDate((String) date.getText());
			mainActivity.selectedSale(cv);
		}
	}

	/**
	 * 
	 * @author rshrestha
	 *
	 */
	public interface OnMainListFragmentInteractionListener {
		public void selectedSale(ClientVisit clientVisit);
	}

	private static final String tag = "SalesListFragment";

}
